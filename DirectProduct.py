#!/usr/bin/env python3
from collections.abc import Iterable
from math import gcd

from Group import Group, GroupElement


class _DirectProductElement(GroupElement):
    def __init__(self, group, elem_1: GroupElement, elem_2: GroupElement):
        if not isinstance(group, DirectProduct):
            raise TypeError('group must be a DirectProduct')
        if elem_1 not in group.group_1:
            raise TypeError(f'{elem_1} is not an element of {group.group_1}')
        if elem_2 not in group.group_2:
            raise TypeError(f'{elem_2} is not an element of {group.group_2}')
        super().__init__(group)
        self.elem_1 = elem_1
        self.elem_2 = elem_2

    def inverse(self):
        return _DirectProductElement(self.group, self.elem_1.inverse(), self.elem_2.inverse())

    def __eq__(self, other):
        if not isinstance(other, _DirectProductElement):
            raise TypeError(f'Cannot compare direct product group element to {type(other)}')
        return self.elem_1 == other.elem_1 and self.elem_2 == other.elem_2

    def __hash__(self):
        return hash((_DirectProductElement, self.elem_1, self.elem_2))

    def __mul__(self, other):
        if not isinstance(other, _DirectProductElement):
            return NotImplemented
        # if other.group.n != self.group.n:
        #     raise ValueError(f'Cannot multiply elements of {self.group} with elements of {other.group}')
        return _DirectProductElement(self.group, self.elem_1 * other.elem_1, self.elem_2 * other.elem_2)

    def short_str(self):
        return f'({self.elem_1.short_str()}, {self.elem_2.short_str()})'


class DirectProduct(Group):

    def __init__(self, group_1, group_2):
        if not isinstance(group_1, Group) or (not isinstance(group_2, Group)):
            raise TypeError(f'DirectProduct arguments must be groups')
        self.group_1 = group_1
        self.group_2 = group_2
        self.identity = _DirectProductElement(self, group_1.identity, group_2.identity)

    @property
    def order(self) -> int:
        return self.group_1.order * self.group_2.order

    @property
    def is_finite(self) -> bool:
        return self.group_1.is_finite and self.group_2.is_finite

    @property
    def is_abelian(self) -> bool:
        return self.group_1.is_abelian and self.group_2.is_abelian

    @property
    def is_cyclic(self) -> bool:
        if self.is_finite:
            # both products must be cyclic and of coprime order
            return (self.group_1.is_cyclic and self.group_2.is_cyclic and
                    gcd(self.group_1.order, self.group_2.order) == 1)
        else:
            # must be Z×C1 or C1×Z
            return ((self.group_1.order == 1 and self.group_2.is_cyclic) or
                    (self.group_2.order == 1 and self.group_1.is_cyclic))

    def __contains__(self, item) -> bool:
        return (isinstance(item, _DirectProductElement)
                and item.elem_1 in self.group_1
                and item.elem_2 in self.group_2)

    def __iter__(self):
        if not isinstance(self.group_1, Iterable) or not isinstance(self.group_2, Iterable):
            raise NotImplementedError
        for elem_1 in self.group_1:
            for elem_2 in self.group_2:
                yield _DirectProductElement(self, elem_1, elem_2)

    def __repr__(self) -> str:
        return f'{self.group_1}×{self.group_2}'
