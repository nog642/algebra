#!/usr/bin/env python3
from Group import Group, GroupElement


class _FiniteCyclicGroupElement(GroupElement):
    def __init__(self, group, val: int):
        if not isinstance(group, FiniteCyclicGroup):
            raise TypeError('group must be a FiniteCyclicGroup')
        super().__init__(group)
        self.val = val

    def inverse(self):
        return _FiniteCyclicGroupElement(self.group, -self.val)

    def __hash__(self):
        return hash((_FiniteCyclicGroupElement, self.group.n, self.val))

    def __eq__(self, other):
        if not isinstance(other, _FiniteCyclicGroupElement):
            raise TypeError(f'Cannot compare finite cyclic group element to {type(other)}')
        if other.group.n != self.group.n:
            raise ValueError(f'Cannot compare elements of C{other.group.n} to elements of C{self.group.n}')
        return self.val == other.val

    def __mul__(self, other):
        if not isinstance(other, _FiniteCyclicGroupElement):
            return NotImplemented
        if other.group.n != self.group.n:
            raise ValueError(f'Cannot multiply elements of {self.group} with elements of {other.group}')
        return _FiniteCyclicGroupElement(self.group, (self.val + other.val) % self.group.n)

    def __pow__(self, power, modulo=None):
        if modulo is not None:
            return NotImplemented
        if not isinstance(power, int):
            return NotImplemented
        return _FiniteCyclicGroupElement(self.group, self.val * power % self.group.n)

    def short_str(self):
        return str(self.val)


class FiniteCyclicGroup(Group):

    def __init__(self, n):
        if not isinstance(n, int):
            raise TypeError(f'n must be int not {type(n)}')
        if n < 1:
            raise TypeError(f'n must be >=1')
        self.n = n
        self.identity = _FiniteCyclicGroupElement(self, 0)
        self.genertor = _FiniteCyclicGroupElement(self, 1)

    @property
    def order(self) -> int:
        return self.n

    @property
    def is_finite(self) -> bool:
        return True

    @property
    def is_abelian(self) -> bool:
        return True

    @property
    def is_cyclic(self) -> bool:
        return True

    def __contains__(self, item) -> bool:
        return isinstance(item, _FiniteCyclicGroupElement) and item.group.n == self.n

    def __iter__(self):
        for i in range(self.n):
            yield _FiniteCyclicGroupElement(self, i)

    def __repr__(self) -> str:
        return f'<Group C{self.n}>'
