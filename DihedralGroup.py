#!/usr/bin/env python3
from Group import Group, GroupElement


class _DihedralGroupElement(GroupElement):
    def __init__(self, group, rotation: int, reflection: bool):
        if not isinstance(group, DihedralGroup):
            raise TypeError('group must be a DihedralGroup')
        super().__init__(group)
        self.rotation = rotation
        self.reflection = reflection

    def inverse(self):
        if self.reflection:
            return self
        else:
            return _DihedralGroupElement(self.group, (-self.rotation) % self.group.n, False)

    def __eq__(self, other):
        if not isinstance(other, _DihedralGroupElement):
            raise TypeError(f'Cannot compare dihedral group element to {type(other)}')
        if other.group.n != self.group.n:
            raise ValueError(f'Cannot compare elements of {other.group} to elements of {self.group}')
        return self.rotation == other.rotation and self.reflection == other.reflection

    def __hash__(self):
        return hash((_DihedralGroupElement, self.group.n, self.rotation, self.reflection))

    def __mul__(self, other):
        if not isinstance(other, _DihedralGroupElement):
            return NotImplemented
        if other.group.n != self.group.n:
            raise ValueError(f'Cannot multiply elements of {self.group} with elements of {other.group}')
        if self.reflection and other.reflection:
            return _DihedralGroupElement(self.group, (other.rotation - self.rotation) % self.group.n, False)
        elif self.reflection and not other.reflection:
            return _DihedralGroupElement(self.group, (other.rotation + self.rotation) % self.group.n, True)
        elif (not self.reflection) and other.reflection:
            return _DihedralGroupElement(self.group, (other.rotation - self.rotation) % self.group.n, True)
        elif (not self.reflection) and not other.reflection:
            return _DihedralGroupElement(self.group, (other.rotation + self.rotation) % self.group.n, False)
        else:
            raise AssertionError

    def short_str(self):
        if not self.rotation:
            if self.reflection:
                return 's'
            else:
                return 'e'
        reflection_str = 's' if self.reflection else ''
        rotation_str = 'r' if self.rotation == 1 else f'r^{self.rotation}'
        return f'{reflection_str}{rotation_str}'


class DihedralGroup(Group):

    def __init__(self, n):
        if not isinstance(n, int):
            raise TypeError(f'n must be int not {type(n)}')
        if n < 1:
            raise TypeError(f'n must be >=1')
        self.n = n
        self.identity = _DihedralGroupElement(self, 0, False)
        self.base_reflection = _DihedralGroupElement(self, 0, True)
        if self.n > 1:
            self.base_rotation = _DihedralGroupElement(self, 1, False)

    @property
    def order(self) -> int:
        return self.n * 2

    @property
    def is_finite(self) -> bool:
        return True

    @property
    def is_abelian(self) -> bool:
        return self.n < 3

    @property
    def is_cyclic(self) -> bool:
        return self.n == 1

    def __contains__(self, item) -> bool:
        return isinstance(item, _DihedralGroupElement) and item.group.n == self.n

    def __iter__(self):
        for i in range(self.n):
            yield _DihedralGroupElement(self, rotation=i, reflection=False)
        for i in range(self.n):
            yield _DihedralGroupElement(self, rotation=i, reflection=True)

    def __repr__(self) -> str:
        return f'<Group D{self.n}>'
