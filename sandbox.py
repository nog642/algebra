#!/usr/bin/env python3
from DirectProduct import DirectProduct
from FiniteCyclicGroup import FiniteCyclicGroup


def main():
    C2 = FiniteCyclicGroup(2)
    V = DirectProduct(C2, C2)
    print(V)
    for g in V:
        print(f'  - {g}')


if __name__ == '__main__':
    main()
