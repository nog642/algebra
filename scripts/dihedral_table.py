#!/usr/bin/env python3
import matplotlib.pyplot as plt
from matplotlib.table import Table

from Group import GroupSubset
from DihedralGroup import DihedralGroup


def table(ax,
          cellText=None, cellColours=None,
          cellLoc='right', colWidths=None,
          rowLabels=None, rowColours=None, rowLoc='left',
          colLabels=None, colColours=None, colLoc='center',
          loc='bottom', bbox=None, edges='closed',
          **kwargs):
    """
    Fixed version of matplotlib.table.table
    """
    if rowLabels is None or colLabels is None:
        raise ValueError('rowLabels and colLabels required')

    if cellColours is None and cellText is None:
        raise ValueError('At least one argument from "cellColours" or '
                         '"cellText" must be provided to create a table.')

    # Check we have some cellText
    if cellText is None:
        # assume just colours are needed
        rows = len(cellColours)
        cols = len(cellColours[0])
        cellText = [[''] * cols] * rows

    rows = len(cellText)
    cols = len(cellText[0])
    for row in cellText:
        if len(row) != cols:
            raise ValueError("Each row in 'cellText' must have {} columns"
                             .format(cols))

    if cellColours is not None:
        if len(cellColours) != rows:
            raise ValueError("'cellColours' must have {} rows".format(rows))
        for row in cellColours:
            if len(row) != cols:
                raise ValueError("Each row in 'cellColours' must have {} "
                                 "columns".format(cols))
    else:
        cellColours = ['w' * cols] * rows

    # Set colwidths if not given
    if colWidths is None:
        colWidths = [1.0 / cols] * cols

    # Fill in missing information for column
    # and row labels
    rowLabelWidth = 0
    if rowLabels is None:
        if rowColours is not None:
            rowLabels = [''] * rows
            rowLabelWidth = colWidths[0]
    elif rowColours is None:
        rowColours = 'w' * rows

    if rowLabels is not None:
        if len(rowLabels) != rows:
            raise ValueError("'rowLabels' must be of length {0}".format(rows))

    # If we have column labels, need to shift
    # the text and colour arrays down 1 row
    offset = 1
    if colLabels is None:
        if colColours is not None:
            colLabels = [''] * cols
        else:
            offset = 0
    elif colColours is None:
        colColours = 'w' * cols

    # Set up cell colours if not given
    if cellColours is None:
        cellColours = ['w' * cols] * rows

    # Now create the table
    table = Table(ax, loc, bbox, **kwargs)
    table.edges = edges
    height = table._approx_text_height()

    # Add the cells
    for row in range(rows):
        for col in range(cols):
            table.add_cell(row + offset, col + 1,
                           width=colWidths[col], height=height,
                           text=cellText[row][col],
                           facecolor=cellColours[row][col],
                           loc=cellLoc)
    # Do column labels
    if colLabels is not None:
        for col in range(cols):
            table.add_cell(0, col + 1,
                           width=colWidths[col], height=height,
                           text=colLabels[col], facecolor=colColours[col],
                           loc=colLoc)

    # Do row labels
    if rowLabels is not None:
        for row in range(rows):
            table.add_cell(row + offset, 0,
                           width=rowLabelWidth or 1e-15, height=height,
                           text=rowLabels[row], facecolor=rowColours[row],
                           loc=rowLoc)
        if rowLabelWidth == 0:
            table.auto_set_column_width(-1)

    ax.add_table(table)
    return table


def latexize(string):
    string = string.replace('{', '\\{').replace('}', '\\}')
    return f'${string}$'


def dihedral_table(n, axes):
    G = DihedralGroup(n)
    e = G.identity
    s = G.base_reflection

    print(G)

    H = GroupSubset(G, {e, s})
    # print(f'H = {H}')

    elements = list(G)
    arr = []
    for g in elements:
        row = []
        for h in elements:
            row.append(g * H * h)
            # print(f'{g.short_str()}H{h.short_str()} = {row[-1].short_str()}')
        arr.append(row)

    left_cosets = frozenset(row[0] for row in arr)
    right_cosets = frozenset(arr[0])

    axes.axis('off')
    axes.set_title('$D_{' + str(n) + '}$')
    ax_table = table(
        axes,
        cellText=[[latexize(item.short_str()) for item in row] for row in arr],
        rowLabels=[latexize(item.short_str()) for item in elements],
        colLabels=[latexize(item.short_str()) for item in elements],
        bbox=[0., 0., 1., 1.]
    )
    for (row, col), cell in ax_table.get_celld().items():
        cell.set_height(1/(2 * n))
        cell.set_width(1/(2 * n))
        cell._text.set_horizontalalignment('center')
        if row == 0 or col == 0:
            continue
        row -= 1
        col -= 1

        subset = arr[row][col]

        if subset in left_cosets and subset in right_cosets:
            cell.set_facecolor('#ff99ff')
        elif subset in left_cosets:
            cell.set_facecolor('#9999ff')
        elif subset in right_cosets:
            cell.set_facecolor('#ff9999')

        if e in subset:
            cell.set_facecolor('#99ff99')

        if elements[row] == elements[col].inverse():
            cell.set_facecolor('#ffff99')


def main():
    plt.rcParams['figure.dpi'] = 300
    plt.rcParams['font.size'] = 24
    num_rows = 5
    fig, axes_arr = plt.subplots(num_rows, 2, figsize=[10., num_rows * 5.])
    n = 1
    for row in axes_arr:
        for axes in row:
            dihedral_table(n, axes)
            n += 1
    plt.show()


if __name__ == '__main__':
    main()
