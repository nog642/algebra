#!/usr/bin/env python3
from collections.abc import Collection
from itertools import product

from better_abc import ABCMeta, abstractattribute, abstractmethod


class Cardinal(metaclass=ABCMeta):

    @abstractmethod
    def __mul__(self, other):
        raise NotImplementedError


Cardinal.register(int)


class GroupElement(metaclass=ABCMeta):

    def __init__(self, group):
        self.group = group

    @abstractmethod
    def __mul__(self, other):
        raise NotImplementedError

    @abstractmethod
    def inverse(self):
        raise NotImplementedError

    @abstractmethod
    def __hash__(self):
        raise NotImplementedError

    def __pow__(self, power, modulo=None):
        if modulo is not None:
            return NotImplemented
        if not isinstance(power, int):
            return NotImplemented
        total_product = self.group.identity
        if power > 0:
            element = self
        elif power < 0:
            power = -power
            element = self.inverse()
        else:
            return total_product
        for _ in range(power):
            total_product *= element
        return total_product

    @abstractmethod
    def short_str(self):
        raise NotImplementedError

    def __repr__(self):
        return f'<GroupElement {self.group} {self.short_str()}>'


class Group(metaclass=ABCMeta):

    @abstractattribute
    def identity(self) -> GroupElement: pass

    @property
    @abstractmethod
    def order(self) -> Cardinal:
        raise NotImplementedError

    @property
    @abstractmethod
    def is_finite(self) -> bool:
        raise NotImplementedError

    @property
    @abstractmethod
    def is_abelian(self) -> bool:
        raise NotImplementedError

    @property
    @abstractmethod
    def is_cyclic(self) -> bool:
        raise NotImplementedError

    @abstractmethod
    def __contains__(self, item) -> bool:
        raise NotImplementedError


def issubgroup(subgroup, group):
    if not isinstance(group, Group):
        raise TypeError('issubgroup second argument argument must be a group')

    if isinstance(subgroup, Group):
        # TODO: get a way to enumerate elements
        return all(element in group for element in subgroup)

    elif isinstance(subgroup, GroupSubset):
        if len(subgroup.elements) == 0:
            # empty subset cannot be a subgroup
            return False

        # check the subgroup is closed
        for elem1, elem2 in product(subgroup.elements, subgroup.elements):
            if (elem1 * elem2) not in subgroup.elements:
                return False
        return True
    else:
        raise TypeError(f'First argument of issubgroup must be Group or GroupSubset, not {type(subgroup)}')


class GroupSubset:

    def __init__(self, group: Group, elements: Collection[GroupElement]):
        for element in elements:
            if not isinstance(element, GroupElement):
                raise TypeError(f'{element} is not a group element')
            if element not in group:
                raise TypeError(f'Group subset of {group} cannot contain element from {element.group}')
        self.group = group
        self.elements = frozenset(elements)

    def __contains__(self, item):
        return item in self.elements

    def __hash__(self):
        return hash((GroupSubset, self.elements))

    def __eq__(self, other):
        if not isinstance(other, GroupSubset):
            raise TypeError
        return self.elements == other.elements

    def __mul__(self, other):  # self * other
        if isinstance(other, GroupElement):
            if other not in self.group:
                raise ValueError(f'Cannot multiply subset of {self.group} by element of {other.group}')
            return GroupSubset(self.group, {element * other for element in self.elements})
        else:
            return NotImplemented

    def __rmul__(self, other):  # other * self
        if isinstance(other, GroupElement):
            if other not in self.group:
                raise ValueError(f'Cannot multiply subset of {self.group} by element of {other.group}')
            return GroupSubset(self.group, {other * element for element in self.elements})
        else:
            return NotImplemented

    def short_str(self) -> str:
        return '{' + ', '.join(sorted(element.short_str() for element in self.elements)) + '}'

    def __repr__(self) -> str:
        return f'<GroupSubset {self.group} {self.short_str()}>'
