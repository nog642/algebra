#!/usr/bin/env python3
import abc
from abc import *

if hasattr(abc, '__all__'):
    __all__ = abc.__all__
else:
    __all__ = [name for name in abc.__dict__ if not name.startswith('_')]
__all__.append('abstractattribute')


class _AbstractAttribute:
    pass


def abstractattribute(_):
    return _AbstractAttribute()


class ABCMeta(ABCMeta):

    def __call__(cls, *args, **kwargs):
        instance = super().__call__(*args, **kwargs)
        abstract_attributes = {name for name in dir(instance)
                               if isinstance(getattr(instance, name), _AbstractAttribute)}
        if abstract_attributes:
            raise NotImplementedError(
                "Can't instantiate abstract class {} with"
                " abstract attributes: {}".format(
                    cls.__name__,
                    ', '.join(abstract_attributes)
                )
            )
        return instance
